import PageController from "../components/navbar/PageController";
import HeaderComponent from "../components/header/HeaderComponent";

export default (
    <cx>
        <div>
            <div>
                <HeaderComponent />
            </div>
            <div>
                <PageController />
            </div>
        </div>
    </cx>
);
