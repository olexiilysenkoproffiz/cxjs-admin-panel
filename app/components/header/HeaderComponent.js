
import "cx/dist/reset.css";
import "cx/dist/widgets.css";
import "./HeaderController.scss";


class HeaderComponent {
    init() {
        super.init();
        this.store.set("$page.tab", "tab5");
    }
}

export default (
    <cx>
        <span style="color: #27aae1;" class="header-container-grid">
            <i class="fas fa-cog fa-3x header-settings-icon"></i>
            <span>
                <input type="text" class="header-search" placeholder="     Search..." />
                <i class="fas fa-search fa-2x header-icon-search"></i>
            </span>
            <span class="header-group">
                <i class="fas fa-plus-square fa-2x"></i>
                <i class="fas fa-table fa-2x"></i>
                <span>
                    <i class="fas fa-circle fa-4x header-user-circle"></i>
                    <p class="header-username">AG</p>
                </span>
            </span>
        </span>
    </cx >
);