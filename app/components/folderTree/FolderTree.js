import { KeySelection } from "cx/ui";
import { Grid, TreeAdapter, TreeNode } from "cx/widgets";

import "cx/dist/reset.css";
import "cx/dist/widgets.css";
import "./FolderTree.scss";

import PageControllerTree from "./Controller";


class FolderTree {
    init() {
        super.init();
    }
}

export default (
    <cx>
        <div controller={PageControllerTree}>
            <Grid
                records: bind="data"
mod="tree"
        style={{ width: "100%" }}
            dataAdapter={{
                type: TreeAdapter,
                load: (context, { controller }, node) => {
                    // TODO: change to show children after click on current record
                    controller.generateSubRecords(node)
                }
            }}
            selection={{ type: KeySelection, bind: "$page.selection" }}
            columns={[
                {
                    field: "folderName",
                    sortable: true,
                    items: (
                        <cx>
                            <div class="treeNode-line">
                                <TreeNode style={{ border: 'solid 1px lightgrey', borderLeft: 'none', paddingBottom: '7px', paddingTop: '7px' }}
                                    expanded: bind="$record.$expanded"
leaf:bind="$record.$leaf"
level:bind="$record.$level"
loading:bind="$record.$loading"
text:bind="$record.folderName"
/> <i class="fas fa-bars" style={{ marginLeft: '-40px', marginTop: '10px' }}></i>
                            </div>
                        </cx>
                    )
                }
            ]}
            />
    </div>
    </cx>
);
