import { Controller } from "cx/ui";

export default class PageControllerTree extends Controller {
    init() {
        super.init();
        this.idSeq = 0;
        this.store.set("data", this.generateRecords());
    }

    generateRecords(node) {
        if (!node || node.$level < 1) {
            let data = [
                {
                    id: ++this.idSeq,
                    folderName: "Corporate",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test2",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test3",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Government Contracts",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "Pleading",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "Protected Materials",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Legislative",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test2",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Litigation",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test2",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Matter-Init",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test2",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test3",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Patent Prosecution And Counseling",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test2",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Personal Workspace",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test2",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test3",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Regulatory",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Tax and Wealth Planning",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "test1",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "test2",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                },
                {
                    id: ++this.idSeq,
                    folderName: "Trademark, Copyright & Licensing",
                    $children: [
                        {
                            id: ++this.idSeq,
                            folderName: "Trademark",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "Copyright",
                            leafIcon: "folder",
                            $leaf: true
                        },
                        {
                            id: ++this.idSeq,
                            folderName: "Licensing",
                            leafIcon: "folder",
                            $leaf: true
                        }
                    ],
                    $leaf: false
                }
            ];
            return data;
        }
    }

    generateSubRecords(node) {
        if (!node || node.$level < 1) {
            let data = node.$children;
            return data;
        }
    }
}
