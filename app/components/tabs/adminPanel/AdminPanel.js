
import { LabelsLeftLayout, KeySelection } from "cx/ui";
import { Button, DateField, TextArea, TextField, Window } from "cx/widgets";

import FolderTree from "../../folderTree/FolderTree";
import FolderSettings from "../../folderSettings/FolderSettings";
import "cx/dist/reset.css";
import "cx/dist/widgets.css";
import "./AdminPanel.scss";

class AdminPanel {
    init() {
        super.init();
    }
}

export default (
    <cx>
        <div class="window-container">
            <div class="administration-tab-header">
                <div class="breadcrumps">
                    <p class="breadcrumps-images"><i class="fas fa-tv"></i> <span id="more-than-sign">></span> <i class="fas fa-tv"></i></p>
                    <p class="template-editor"> Template Editor</p>
                </div>
                <div class="window-actions">
                    <i class="fas fa-bars"></i> <i class="fas fa-times-circle"></i>
                </div>
            </div>
            <div class="panel-container">
                <div class="template-list">
                    <div class="template-list-header">
                        <div>
                            Template List
                    </div>
                        <Button
                            class="new-template-button"
                            mod="primary"
                        >
                            <i class="fas fa-plus plus-create-template"></i>
                            Create Template
                    </Button>
                    </div>
                    <FolderTree />
                </div>
                <div class="item-form">
                    <div class="item-form-tabs">

                    </div>
                    <FolderSettings />
                </div>
            </div>
        </div>
    </cx>
);
