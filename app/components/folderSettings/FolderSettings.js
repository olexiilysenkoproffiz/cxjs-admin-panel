import { LabelsLeftLayout } from "cx/ui";
import { computable } from 'cx/data';
import { Select } from 'cx/widgets';
import {
    Button,
    Radio,
    MsgBox,
    TextField,
    ValidationGroup
} from "cx/widgets";
import { Controller } from "cx/ui";
import { Tab } from "cx/widgets";



import "cx/dist/reset.css";
import "cx/dist/widgets.css";
import "./FolderSettings.scss";


class FolderSettings extends Controller {
    init() {
        super.init();
        this.store.set("$page.tabs", "tab7", "$page.selection");
    }
}


export default (
    <cx>
        <div class="widgets settings" controller={FolderSettings}>

            <div style="margin:10px; width:100%;">
                <div style="height: 39px; margin-top:-27px;margin-left:-33px;padding-left:10px;white-space:nowrap;width:100%; border-top: solid 1px lightgrey;border-bottom: solid 1px lightgrey;">

                    <div class="navbar-settings">
                        <span style={{ marginLeft: '300px' }}>|</span>
                        <Tab tab="tab7" value: bind="$page.tabs" mod="classic" baseClass="subtab-headers">
                            Basic
        </Tab> <span>|</span>
                    <Tab tab="tab8" value: bind="$page.tabs" mod="classic" baseClass="subtab-headers">
                        Security
        </Tab><span>|</span>
                <Tab tab="tab9" value: bind="$page.tabs" mod="classic" baseClass="subtab-headers">
                    Metadata
        </Tab><span>|</span>
            <Tab tab="tab10" value: bind="$page.tabs" mod="classic" baseClass="subtab-headers" >
            Avaluability
        </Tab>          
                        </div>
    </div >
    <div style="background: #fff; padding: 15px; height: 400px;z-index:1;margin-top:2px;">
        <div visible: expr="{$page.tabs}=='tab7'">
        <form
            class="settings-form"
            // TODO: move this handler to the parent
            onSubmit={(e, { store }) => {
                e.preventDefault();
                MsgBox.alert(`Thank you for add/edit this folder!`);
                store.set("folderName", null);
            }}
        >
            <ValidationGroup layout={LabelsLeftLayout} invalid: bind="login.invalid">
        <TextField style={{ width: '500px' }}
                // value: bind="login.username"
                //value-bind="data[$page.selection].folderName"
                value={computable('data', '$page.selection', (data, recordId) =>
                    (recordId) ? data.find((obj) => {
                        return (
                            obj.id === recordId ? obj.id === recordId : obj.$children.find((child) => {
                                return (child.id === recordId)
                            })
                        )
                    }).folderName : ""
                )}
                label="Folder name"
                required={true}
            />
            <Select style={{ width: '500px' }}
                value="1" label="Folder tepe">
                <option value={1}>Folder</option>
                <option value={2}>File</option>
            </Select>
            <div class="show-as-radio">
                <Radio
                    label="Show As"
                    value: bind="$page.option"
option="0"
text="Checked"
name="showAs"
/>
      <Radio
                    value: bind="$page.option"
option="1"
text="Required"
name="showAs"
/>
      <Radio
                    value: bind="$page.option"
option="2"
name="group2"
text="None"
name="showAs"
/>
       </div>
            <div class="prefix-sufix-radio">
                <Radio
                    label="Prefix/Sufix"
                    value: bind="$page.option"
option="3"
name="group2"
text="Allow Prefix"
name="prefixSufix"
/>

      <Radio
                    value: bind="$page.option"
option="4"
name="group2"
text="Allow Custom Name"
name="prefixSufix"
/>
       </div>
            <div class="suffix-radio">
                <Radio
                    label="Allow suffix"
                    value: bind="$page.option"
option="5"
text=""
name="suffix"
/>

      <Radio
                    value: bind="$page.option"
option="6"
name="group2"
text="None"
name="suffix"
/>
       </div>
            <Button
                class="save-btn"
                mod="primary"
                disabled: bind="login.invalid"
submit
><i class="fas fa-save"></i>
            Save
        </Button>
      </ValidationGroup >
    </form >

            </div >
    <div visible: expr="{$page.tabs}=='tab8'">Tab 8</div>
    <div visible: expr="{$page.tabs}=='tab9'">Tab 9</div>
    <div visible: expr="{$page.tabs}=='tab10'">Tab 10

    </div >
    </div >
</div >
</div >


   
  </cx >
);
