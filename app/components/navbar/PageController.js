import { Controller } from "cx/ui";
import { Tab } from "cx/widgets";

import AdminPanel from "../tabs/adminPanel/AdminPanel";

import "cx/dist/reset.css";
import "cx/dist/widgets.css";
import "./PageController.scss";

class PageController extends Controller {
    init() {
        super.init();
        this.store.set("$page.tab", "tab5");
    }
}

export default (
    <cx>
        <div class="widgets" controller={PageController}>

            <div style="margin:10px; width:100%;">
                <div style="padding-left:10px;white-space:nowrap;width:100%;">
                    <Tab tab="tab1" value: bind="$page.tab" mod="classic">
                        Matters
                    </Tab>
                <Tab tab="tab2" value: bind="$page.tab" mod="classic">
                    Jobs
                    </Tab>
            <Tab tab="tab3" value: bind="$page.tab" mod="classic">
                Users & Groups
                    </Tab>
        <Tab tab="tab4" value: bind="$page.tab" mod="classic" >
            Approvals
                    </Tab>
    <Tab tab="tab5" value: bind="$page.tab" mod="classic" >
        Administration
                    </Tab>
    <Tab tab="tab6" value: bind="$page.tab" mod="classic" >
        Analitics
                    </Tab>
                </div >
    <div style="border: 2px solid #26aae2; background: #b6e2f5; padding: 15px; height: 600px;z-index:1;margin-top:-1px;">
        <div visible: expr="{$page.tab}=='tab1'">Tab 1</div>
    <div visible: expr="{$page.tab}=='tab2'">Tab 2</div>
    <div visible: expr="{$page.tab}=='tab3'">Tab 3</div>
    <div visible: expr="{$page.tab}=='tab4'">Tab 4</div>
    <div visible: expr="{$page.tab}=='tab5'">
        <AdminPanel />
    </div>
    <div visible: expr="{$page.tab}=='tab6'">Tab 6</div>
                </div >
            </div >
        </div >
    </cx >
  );

